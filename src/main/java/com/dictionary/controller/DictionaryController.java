package com.dictionary.controller;

import com.dictionary.entity.Word;
import com.dictionary.form.DetailForm;
import com.dictionary.form.ErrorMessage;
import com.dictionary.form.SearchRequest;
import com.dictionary.repository.WordRepository;
import com.dictionary.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.util.StringUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Controller
public class DictionaryController {

  @Autowired
  private WordRepository wordRepository;

  @Autowired
  private WordService wordService;

  @GetMapping("/")
  public String homePage(Model model) {
    DetailForm detailForm = new DetailForm();
    model.addAttribute("detailForm", detailForm);
    return "homepage";
  }

  @GetMapping("/insert-db")
  public String insertDb(Model model) throws FileNotFoundException {
    String url = "F:\\av.txt";
    FileInputStream fileInputStream = new FileInputStream(url);
    Scanner scanner = new Scanner(fileInputStream);
    List<String> allWords = new ArrayList<>();

    try {
      while (scanner.hasNextLine()) {
        String word = scanner.nextLine();
        if (!word.contains("###") && word.contains("##") && word.contains("#*") && word.contains("|-") && word.length() < 255) {
          allWords.add(word);
        }
      }
    } finally {
      try {
        scanner.close();
        fileInputStream.close();
      } catch (IOException ex) {
        System.out.println(ex.getMessage());
      }
    }
    List<Word> wordList = new ArrayList<>();

    for (String str: allWords) {
      String s = str.substring(0, str.indexOf("##"));
      String s1 = str.substring(str.indexOf("##") + 2, str.indexOf("#*"));
      String s2 = str.substring(str.indexOf("#*") + 2, str.indexOf("|-"));
      String s3 = str.substring(str.indexOf("|-") + 2);

      Word word = new Word();
      word.setWordTarget(s);
      word.setSpelling(s1);
      word.setWordType(s2);
      word.setWordExplain(s3);

      wordList.add(word);
    }
    wordRepository.saveAll(wordList);
    DetailForm detailForm = new DetailForm();
    model.addAttribute("detailForm", detailForm);
    return "homepage";
  }

  @PostMapping("/dictionary/search")
  public String searchWord(Model model, @ModelAttribute(name = "detailForm") DetailForm detailForm,
    RedirectAttributes redirectAttributes) {
    Word word = wordRepository.findByWordTarget(detailForm.getWord().getWordTarget());
    List<Word> wordRelates = wordRepository.findByWordTargetIsContaining(detailForm.getWord().getWordTarget());
    wordRelates.remove(word);
    List<Word> top5WordRelates = new ArrayList<>();
    for (int i = 0; i < 10; i++) {
      if (i > wordRelates.size() - 1) {
        break;
      }
      if (!ObjectUtils.isEmpty(wordRelates.get(i))) {
        top5WordRelates.add(wordRelates.get(i));
      }
    }
    wordRelates.remove(word);
    model.addAttribute("result", word);
    if (ObjectUtils.isEmpty(word)) {
      redirectAttributes.addFlashAttribute("isResult", true);
    }
    redirectAttributes.addFlashAttribute("result", word);
    redirectAttributes.addFlashAttribute("relateWords", top5WordRelates);
    redirectAttributes.addFlashAttribute("detailForm", detailForm);
    return "redirect:/";
  }

  @GetMapping("/dictionary/search/{wordRelate}")
  public String searchWordByRelate(Model model, @PathVariable(name = "wordRelate") String wordRelate,
    RedirectAttributes redirectAttributes) {
    DetailForm detailForm = new DetailForm();
    Word word = wordRepository.findByWordTarget(wordRelate);
    List<Word> wordRelates = new ArrayList<>();;
    model.addAttribute("result", word);
    redirectAttributes.addFlashAttribute("result", word);
    redirectAttributes.addFlashAttribute("relateWords", wordRelates);
    redirectAttributes.addFlashAttribute("detailForm", detailForm);
    return "redirect:/";
  }

  @GetMapping("/dictionary/list")
  public String listWord(Model model) {
//    List<Word> wordList = wordRepository.findAll();
    Page<Word> wordList = wordService.listWord();
    SearchRequest searchRequest = new SearchRequest();
    model.addAttribute("searchRequest", searchRequest);
    model.addAttribute("wordList", wordList);
    return "list";
  }

  @PostMapping("/dictionary/list/search")
  public String listWordSearch(Model model, SearchRequest searchRequest) {
    List<Word> wordList = wordRepository.findByWordTargetIsContaining(searchRequest.getSearchTxt());
    model.addAttribute("wordList", wordList);
    return "list";
  }

  @PostMapping("/dictionary/list/delete/")
  public String delete(Model model, @RequestParam(name = "id") String id) {
    if (!StringUtils.isEmpty(id)) {
      wordRepository.deleteById(Long.parseLong(id));
    }
    return "redirect:/dictionary/list";
  }

  @GetMapping("/dictionary/register/")
  public String insertProduct(Model model) {
    model.addAttribute("isInsert", true);
    ErrorMessage errorMessage = new ErrorMessage();
    DetailForm detailForm = new DetailForm();
    Word word = new Word();
    detailForm.setWord(word);
    model.addAttribute("detailForm", detailForm);
    model.addAttribute("errorMessage", errorMessage);
    return "detail";
  }

  @PostMapping("/dictionary/register/confirm")
  public String insertConfirm(
    Model model, @ModelAttribute(name = "detailForm") DetailForm detailForm,
    @ModelAttribute(name = "errorMessage") ErrorMessage errorMessage) {
    model.addAttribute("isInsert", true);
    Word word = detailForm.getWord();
    if (!StringUtils.isEmpty(word.getWordTarget()) && !StringUtils
      .isEmpty(word.getWordExplain()) && !StringUtils
      .isEmpty(word.getWordType())) {
      if (!ObjectUtils.isEmpty(wordRepository.findByWordTarget(word.getWordTarget()))) {
        errorMessage.setMessage("Đã tồn tại từ trong DB");
        return "detail";
      } else {
        if (wordService.addNew(detailForm)) {
          return "redirect:/dictionary/list";
        }
      }
    }
    errorMessage.setMessage("Hãy điền đầy đủ");
    return "detail";
  }

  @GetMapping("/dictionary/list/edit/{id}")
  public String edit(Model model, @PathVariable(name = "id") String id) {
    model.addAttribute("isInsert", false);
    Word word = wordService.findByIdWord(id);
    ErrorMessage errorMessage = new ErrorMessage();
    DetailForm detailForm = new DetailForm();
    detailForm.setWord(word);
    model.addAttribute("detailForm", detailForm);
    model.addAttribute("errorMessage", errorMessage);
    return "detail";
  }

  @PostMapping("/dictionary/edit/confirm")
  public String editConfirm(
    Model model, @ModelAttribute(name = "detailForm") DetailForm detailForm,
    @ModelAttribute(name = "errorMessage") ErrorMessage errorMessage) {
    model.addAttribute("isInsert", false);
    Optional<Word> word = wordRepository.findById(detailForm.getWord().getId());
    Word wordCheck = wordRepository.findByWordTarget(detailForm.getWord().getWordTarget());
    if (!ObjectUtils.isEmpty(wordCheck) && word.isPresent() && !word.get().getId().equals(wordCheck.getId())) {
      errorMessage.setMessage("Đã tồn tại trong DB");
      return "detail";
    }
    if (word.isPresent() && !StringUtils.isEmpty(word.get().getWordTarget()) && !StringUtils
      .isEmpty(word.get().getWordExplain()) && !StringUtils
      .isEmpty(word.get().getWordType())) {
      word.get().setWordExplain(detailForm.getWord().getWordExplain());
      word.get().setWordTarget(detailForm.getWord().getWordTarget());
      word.get().setSpelling(detailForm.getWord().getSpelling());
      word.get().setWordType(detailForm.getWord().getWordType());
      wordRepository.save(word.get());
      return "redirect:/dictionary/list";
    }
    errorMessage.setMessage("Hãy điền đầy đủ");
    return "detail";
  }

}
