package com.dictionary.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Word {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String wordTarget;

  private String wordExplain;

  private String spelling;

  private String wordType;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getWordTarget() {
    return wordTarget;
  }

  public void setWordTarget(String wordTarget) {
    this.wordTarget = wordTarget;
  }

  public String getWordExplain() {
    return wordExplain;
  }

  public void setWordExplain(String wordExplain) {
    this.wordExplain = wordExplain;
  }

  public String getSpelling() {
    return spelling;
  }

  public void setSpelling(String spelling) {
    this.spelling = spelling;
  }

  public String getWordType() {
    return wordType;
  }

  public void setWordType(String wordType) {
    this.wordType = wordType;
  }
}
