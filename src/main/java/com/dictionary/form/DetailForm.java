package com.dictionary.form;

import com.dictionary.entity.Word;

public class DetailForm{

  private Word word;

  public Word getWord() {
    return word;
  }

  public void setWord(Word word) {
    this.word = word;
  }
}
