package com.dictionary.form;

public class SearchRequest {
  private String searchTxt;

  public String getSearchTxt() {
    return searchTxt;
  }

  public void setSearchTxt(String searchTxt) {
    this.searchTxt = searchTxt;
  }
}
