package com.dictionary.repository;

import com.dictionary.entity.Word;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WordRepository extends JpaRepository<Word, Long> {

  Word findByWordTarget(String wordTarget);

  List<Word> findByWordTargetIsContaining(String wordTarget);


}
