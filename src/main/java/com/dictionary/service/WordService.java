package com.dictionary.service;

import com.dictionary.entity.Word;
import com.dictionary.form.DetailForm;
import com.dictionary.repository.WordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class WordService {

  @Autowired
  private WordRepository wordRepository;

  public boolean addNew(DetailForm detailForm) {
    Word word = detailForm.getWord();
    if (!ObjectUtils.isEmpty(wordRepository.save(word))) {
      return true;
    }
    return false;
  }

  public Word findByIdWord(String id) {
    return wordRepository.findById(Long.parseLong(id))
      .orElse(new Word());
  }

  public Page<Word> listWord(){
    Pageable firstPageWithTwoElements = PageRequest.of(0, 100);
    Page<Word> words = wordRepository.findAll(firstPageWithTwoElements);
    return words;
  }
}
